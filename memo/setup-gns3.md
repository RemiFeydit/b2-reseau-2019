# Sommaire

* [Setup GNS3](#setup-gns3)
* [Get router images](#get-router-images)
* [Get and setup IOU](#get-and-setup-iou)
* [Utilisation d'un VPCS](#utilisation-dun-vpcs)

# Setup GNS3

Prérequis : 
* VirtualBox ou autre hyperviseur

Steps :
* installer GNS3 depuis le site officiel
* récupérer la GNS3 VM depuis le site officiel
* importer la GNS3 VM dans votre hyperviseur
* vérifier que GNS3 et la GNS3 VM sont dans la même version
  * sinon se rendre sur [la page de releases GN3 sur Github](https://github.com/GNS3/gns3-gui/releases) pour récupérer une version spécifique de l'un ou l'autre
* configurer GNS3
  * Edit > Preferences > Server
    * "Host Binding" : sélectionner l'IP du même host-only que celui utilisé par la GNS3 VM

# Get router images

Des images de routeurs IOS sont dispos ici : [là](https://drive.google.com/drive/folders/102jxZ9ECpe6ZFtXYdK_81iEVuuFoGOGR).

Pour ajouter les images dans GNS3 :
* Edit > Preferences > IOS Routers

# Get and setup IOU

L'IOU L2 qu'on va utiliser permet de virtualiser le même logiciel qu'utilise certains switches Cisco.

L'image IOU est dispo ici : [là](https://drive.google.com/drive/folders/1LBIlztgGVAk4XsAeovKb1JHBrRg8QPSz), c'est le `.bin`.

Pour importer l'IOU : 
* Edit > Preferences > IOU Devices
  * importer le `.bin`
* Edit > Preferences > IOS on UNIX
  * copier ce qui suit : 
```
[license]
localhost.localdomain = 73635fd3b0a13ad0;
gns3vm = 73635fd3b0a13ad0;
```

# Utilisation d'un VPCS

Les VPCS (Virtual PC Simulator) dans GNS3 permettent de simuler des clients. Plutôt que virtualiser un vrai OS, comme un Linux, ce petit outil permet de profiter des fonctionnalités élémentaires de réseau. 

Dans GN3, section "End Devices", il y a VPCS. Simple drag'n'drop, puis vous pouvez l'allumer.

Double-clic sur le VPCS pour pop une console.

La ligne de commande est très simple et est similaire (dans l'utilisation) à une ligne de commande Cisco. 

Set une ip : 
```
PC-1> ip 10.1.1.10/24
```

Voir table ARP : 
```
PC-1> show arp
```

Utilisez `?` pour avoir une liste des commandes et l'auto-complétion (comme le CLI Cisco)